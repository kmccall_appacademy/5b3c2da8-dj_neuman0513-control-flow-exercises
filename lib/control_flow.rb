# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char {|let| str.delete!(let) if let == let.downcase}
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"

def middle_substring(str)
idx = str.length / 2
  if str.length.even?
      str[idx - 1..idx]
  else
      str[idx]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowels = "aeiou"

  str.count(vowels)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
(1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
string = ''
  arr.each do |let|
  string << let
  string << separator unless let == arr.last
  end
string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
chars_arr = str.chars
result = ""
chars_arr.each_with_index do |let, idx|
  if (idx + 1).odd?
    result << let.downcase
  else
    result << let.upcase
  end
end
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  word_arr = str.split

  word_arr.each do |word|
    word.reverse! if word.length >= 5
  end
word_arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
(1..n).each do |num|
  if num % 3 == 0  && num % 5 == 0
    result << "fizzbuzz"
  elsif num % 3 == 0
    result << "fizz"
  elsif num % 5 == 0
    result << "buzz"
  else
    result << num
  end
end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse = arr.reverse
  result = []
  reverse.each{|num| result << num}
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
#prime numbers can only be divided by one and itself evenly
#loop through num beginning with 1
def prime?(num)
return false if num == 1

(2...num).each do |n|
  if num % n == 0
    return false
  end
end
true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
result = [].sort
(1..num).each{|n| result << n if num % n == 0 }
result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
factors = factors(num)
result = []
factors.each {|x| result << x if prime?(x)}
result
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
odds = []
evens = []

arr.each do |x|
  x.odd? ? odds << x : evens << x
end

return odds[0] if odds.count == 1
return evens[0] if evens.count == 1
end
